import { DynamicModule, Module,  } from '@nestjs/common';
import { ApmService } from './apm.service';

@Module({
  providers: [ApmService],
  exports: [ApmService],
})
export class ApmModule {
  static forRoot(options): DynamicModule {
    return {
      module: ApmModule,
      providers: [ApmService],
      exports: [ApmService],
    }
  }
}
